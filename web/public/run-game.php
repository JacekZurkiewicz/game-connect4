<?php
use Ratchet\Server\IoServer;
use Noodlehaus\Config;
use Noodlehaus\Parser\Yaml;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;

include('/var/www/html/app/vendor/autoload.php');

/*
 *---------------------------------------------------------------
 * ŚRODOWISKO APLIKACJI
 *---------------------------------------------------------------
 *
 * Aplikacja może załadować różne ustawienia.
 * Jest to zależne tylko od ustawień serwera.
 * W pliku konfiguracyjnym są zdefiniowane wszystkie zmienne,
 * które mogą uledz zmianie w zależności od wykorzystywanego środowiska.
 *
 * Obecnie rozróżniamy środowiska:
 *      produkcyjne
 *      pozostałe
 *
 * System pobiera z serwera parametr 'CONNECT4_ENVIRONMENT'
 * od którego uzależonony jest rodzaj załadowanej konfigracji.
 * Jeśli parametr na serwerze:
 *
 * NIE jest skonfigurowany system wybiera standardową nazwę konfiguracji
 * JEST skonfigurowany system wybiera nazwę konfiguracji proponowanej przez serwer.
 *
 * Po pobraniu nazwy konfigracji system tworzy nazwę pliku z konfiguracją.
 *
 * Kolejnym krokiem jest sprawdzenie przez system
 * czy plik o takiej nazwie znajduje się na serwerze
 * w katalogu (__DIR__ . '/../src/config/')
 *
 * Jeśli:
 *  TAK - wybrana konfiguracja jest ładowana
 *  NIE - Wyrzucany jest wyjątek o braku wybranej konfiguracji
 *
 *
 */

$format = "[%datetime%] [%file%] [%level_name%] %message% [Context %context% Extra %extra%]\n";

$logger = new Logger('run-game');

$dataFormat = "d/m/Y H:i:s";
$loggerTimeZone = new DateTimeZone('Europe/Warsaw');
$format = "[%datetime%] [%file%] [%level_name%] %message% [Context %context% Extra %extra%]\n";

$formatter = new LineFormatter($output, $dataFormat);

$stream = new StreamHandler('/var/www/html/public/log/connect4-game.log', Logger::INFO);
$stream->setFormatter($formatter);
$logger->setTimezone($loggerTimeZone);
$logger->pushHandler($stream);
$logger->info('Start run-game');

$env = getenv('CONNECT4_ENVIRONMENT');
$configFilePath ='/var/www/html/app/src/config/';

if(!$env){
    $configFilename = 'prod.yml';
} else {
    $configFilename = $env.'.yml';
}

if(!file_exists($configFilePath.$configFilename)){
    throw new \Exception('Plik konfiguracyjny '. $configFilePath.$configFilename.' nie istnieje');
}

$server = IoServer::factory(
    new \Ratchet\Http\HttpServer(
        new \Ratchet\WebSocket\WsServer(
            new \Connect4\Classes\ConnectGame(
                new Config($configFilePath.$configFilename, new Yaml),
                new Predis\Client([
                    'scheme' => 'tcp',
                    'host'   => 'redis',
                    'port'   => 6379,
                ]),
                $logger
            )
        )
    ),
    8085,
    '0.0.0.0'
);

$server->run();
