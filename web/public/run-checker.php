<?php

//wykorzysta battle chcecker
//wysylka do ws identyfikatora battle dla ktorych mamy wykonac checkdedline.

use Connect4\utils\BattleChecker;
use Connect4\utils\Json;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;
use Noodlehaus\Config;
use Noodlehaus\Parser\Yaml;


$configFilePath ='/var/www/html/app/src/config/';

$env = getenv('CONNECT4_ENVIRONMENT');

if(!$env){
    $configFilename = 'prod.yml';
} else {
    $configFilename = $env.'.yml';
}

include('/var/www/html/app/vendor/autoload.php');

if(!file_exists($configFilePath.$configFilename)){
    throw new \Exception('Plik konfiguracyjny '. $configFilePath.$configFilename.' nie istnieje');
} else {
    $config = $configFilePath.$configFilename;
}

$battle = new BattleChecker(
    new Predis\Client([
        'scheme' => 'tcp',
        'host'   => 'redis',
        'port'   => 6379,
    ])
);
$config = new Config($configFilePath.$configFilename, new Yaml);

$logger = new Logger('connect4-game');

$dataFormat = "d/m/Y H:i:s";
$format = "[%datetime%] [%file%] [%level_name%] %message% [Context %context% Extra %extra%]\n";

$formatter = new LineFormatter($output, $dataFormat);

$stream = new StreamHandler('/var/www/html/public/log/connect4-checker.log', Logger::INFO);
$stream->setFormatter($formatter);
$logger->pushHandler($stream);


// todo - dodac logger to konfiguracji
$logger->info('starting checker');

$topic = $config->get('checker.topic');

$mainLoop = \React\EventLoop\Factory::create();

\Ratchet\Client\connect('ws://php:8085',  [], [], $mainLoop)->then(function($conn) use (
    &$logger, &$battle, &$topic, &$mainLoop
){
    $mainLoop->addPeriodicTimer(5, function() use(&$logger, &$battle, &$topic, &$conn){
        $battlesId = $battle->getDeadlinedBattlesId();
        foreach ($battlesId as $battleId) {
            $logger->info('[run-checker] - wysyłam info o przeterminowym battle - battle id: '.$battleId);

            $conn->send(
                Json::encode(
                    array(
                        'topic' => $topic,
                        'payload' => array(
                            'id' => $battleId,
                        )
                    )
                )
            );
        }
    });
}, function ($e) use (&$logger) {
    $logger->info('could not connect :'.$e->getMessage());
    $logger->critical('could not connect :'.$e->getMessage());
});

$mainLoop->run();