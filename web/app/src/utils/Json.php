<?php

namespace Connect4\utils;


class Json
{
    /**
     * Json constructor.
     */
    protected function __construct()
    {
    }

    /**
     *
     * Konwertowanie na JSON
     *
     * @param $data
     * @return string
     * @throws \Exception
     */
    public static function encode($data)
    {
            return json_encode(
                self::validate($data, 'encode')
            );

    }

    /**
     *
     * dekodowanie JSON
     *
     * @param $json
     * @param bool $assoc
     * @return mixed
     */
    public static function decode($json, $assoc = false)
    {
        return json_decode($json, $assoc);
    }

    /**
     *
     * Sprawdzanie czy dane zawieraja poprawna strukture.
     * Czyli zawieraja topic i payload
     *
     * @param $data
     * @param $type
     * @return mixed
     * @throws \Exception
     */
    private static function validate($data, $type){
        switch ($type){
            case 'encode':
                if (!array_key_exists('topic', $data) ||
                    !array_key_exists('payload', $data)){
                    throw new \Exception(
                        'Niepoprawna struktura danych wejściowych'
                    );
                }

                return $data;
            case 'decode':
                return $data;
            default:
                throw new \Exception('JSON nie posiada aktualnie obsługi walidacji żądanego typu.');
                break;
        }
    }
}