<?php

namespace Connect4\utils;

use Predis\Client;


class BattleChecker
{

    private $redis;

    /**
     * BattleChecker constructor.
     * @param Client $redis
     */
    public function __construct(Client $redis)
    {
        $this->redis = $redis;

    }

    /**
     *
     * Sprawdzanie na podstawie listy rozgrywek pobranych z redis, które z nich przekroczyły czas currentDeadline
     *
     * @return array
     */
    public function getDeadlinedBattlesId()
    {
        $battlesList = array();
        $battles = $this->redis->keys('battles:*');

        foreach ($battles as $battleHash){
            $battleData = $this->redis->hgetall($battleHash);

            $currentDeadline = $battleData["currentDeadline"];

            $actualTime = time();

            if($actualTime >= $currentDeadline && !empty($currentDeadline)){
                try{
                    $battlesList[] = $this->getBattleId($battleHash);
                } catch (\Exception $exception){
                    //todo - logowanie bledu do pliku
                }
            }
        }

        return $battlesList;
    }

    /**
     *
     * Zwraca identyfikator rozgrywki
     *
     * @param $battleHash
     * @return mixed
     * @throws \Exception
     */
    private function getBattleId($battleHash)
    {
        $battleId = explode(":", $battleHash);

        if(isset($battleId[1])){
            return $battleId[1];
        } else {
            throw new \Exception('Błędny numer rozgrywki');
        }
    }
}