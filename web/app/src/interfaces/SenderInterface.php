<?php

namespace Connect4\interfaces;

interface SenderInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function send($data);
}