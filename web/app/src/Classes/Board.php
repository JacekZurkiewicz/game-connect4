<?php

namespace Connect4\Classes;


use Connect4\exceptions\board\BoardException;
use Noodlehaus\Config;
use phpDocumentor\Reflection\Types\Integer;

class Board
{
    public $matrix;


    private $config;
    private $size;
    private $move;

    /**
     * Board constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
        $this->initializeParams();
        $this->initializeBoard();
    }


    private function initializeParams()
    {
        $this->size = $this->config->get('board.size');
    }

    /**
     *
     * Pobranie rozmiaru tablicy
     *
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @return mixed
     */
    public function getMove() : PlayerMove
    {
        return $this->move;
    }

    /**
     * @param $move
     */
    private function setMove(PlayerMove $move) :void
    {
        $this->move = $move;
    }

    /**
     * inicjalizacja planszy na podstawie zdefiniowanego rozmiaru
     */
    private function initializeBoard() : void
    {
        for($i = 1; $i<=$this->getSize(); $i++){
            for($j = 1; $j<=$this->getSize(); $j++){
                $this->matrix[$i][$j] = array();
            }
        }
    }

    /**
     * @return mixed
     */
    public function getFormattedMatrix() : array
    {
        return $this->matrix;
    }

    public function makeMove(PlayerMove $move)
    {
        $this->setMove($move);

        /**
         *
         * utworzenie loga w klasie battle do ktorego bedzie dopisana cala hostoria z rozgrywka
         *
         *
         */

        $this
            ->validateColumn()
            ->insertPill();

        return true;

    }

    /**
     *
     * Sprawdzenie czy przesłany numer kolumny nie jest większy niż jej rozmiar
     *
     * @return Board
     * @throws BoardException
     */
    private function validateColumn() : Board
    {
        if ($this->getMove()->getColumn() > $this->getSize()){
            throw new BoardException('Błędny numer kolumny', 5);
        }

        return $this;
    }

    private function insertPill() : Board
    {

        $this->findRowKey();

        $this->matrix
        [$this->getMove()->getRow()]
        [$this->getMove()->getColumn()]
            = $this->getMove();


//        wybrac komorke rowKey i do niej wstawic
//        do tego musi byc dodany log o ktorym rozmawialismy.
//        zwraca numerplayer, timestamp oraz uzuplenic loga - ma on byc w obiekcie board


        // TODO - zastanowic sie nad logiem ruchow - moze klasa


        return $this;
    }

    /**
     *
     * Ustawia znaleziony numer wiersza do którego trafił wrzucony żeton jako właściwość klasy PlayerMove
     *
     * @throws BoardException
     */
    private function findRowKey()
    {
        for($x=1;$x<=$this->getSize();$x++){
            if (!empty($this->matrix[$x][$this->getMove()->getColumn()])){

                if ($x === 1){
                   throw new BoardException('Zapełniona kolumna', 4);
                } else {
                    $findedRow = $x-1;
                    $this->getMove()->setRow($findedRow);
                    return $findedRow;
                }
            }
        }
        $this->getMove()->setRow($this->getSize());
        return $this->getSize();
    }
}