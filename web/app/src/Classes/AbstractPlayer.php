<?php

namespace Connect4\Classes;

use Connect4\exceptions\NotObject;
use Connect4\exceptions\NotImplementedException;
use Connect4\interfaces\SenderInterface;
use Monolog\Logger;
use Ratchet\ConnectionInterface;
use Noodlehaus\Config;


abstract class AbstractPlayer implements SenderInterface
{
    private $defaultNickLength;

    private $battle = null;
    private $config;
    private $id;
    private $nick;
    private $logger;

    protected $connection;

    /**
     * AbstractPlayer constructor.
     * @param int $resourceId
     * @param $conn
     * @param Config $config
     * @param Logger $logger
     * @throws NotImplementedException
     * @throws NotObject
     * @throws \ReflectionException
     */
    public function __construct(int $resourceId, $conn, Config $config, Logger $logger)
    {
        $this->config = $config;
        $this->initializeParams();
        $this->connection = $this->validateConnection($conn);
        $this->nick = $this->generateNick();
        $this->id = $resourceId;
        $this->logger = $logger;
    }

    private function initializeParams()
    {
        $this->defaultNickLength = $this->config->get('player.defaultNickLength');
    }

    /**
     *
     * Getter właściwości id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * Geter wiłaściwości nick
     *
     * @return string
     */
    public function getNick() :string
    {
        return $this->nick;
    }

    public function getDefaultNickLength()
    {
        return $this->defaultNickLength;
    }

    /**
     *
     * Wysłanie danych do zawodnika
     *
     * @param $data
     * @return mixed
     * @throws \Exception
     */
    public function send($data)
    {
        return $this->connection->send($data);
    }

    /**
     *
     * generowanie nick
     *
     * @return string
     */
    private function generateNick()
    {
        $characters =
            '0123456789abcdefghijklmnoprstuwxyzABCDEFGHIJKLMNOPRSTWXYZ'
        ;
        $charactersLength = strlen($characters);
        $randomString = '';

        for($i = 0 ; $i < $this->getDefaultNickLength(); $i++){
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    /**
     * Validate Connection
     *
     * @param $connection
     * @return mixed
     * @throws NotImplementedException
     * @throws NotObject
     * @throws \ReflectionException
     */
    private function validateConnection($connection)
    {
        if(is_object($connection)){
            if($connection instanceof ConnectionInterface){
                return $connection;
            }

            $reflectedConnectionClass = new \ReflectionClass($connection);
            throw new NotImplementedException(
                'Brak implementacji dla klasy: ' .
                $reflectedConnectionClass->getShortName()
            );
        }
        throw new NotObject('Przesłana wartość nie jest obiektem');
    }

    /**
     *
     * Pobranie rozgrywki
     *
     * @return Battle
     */
    public function getBattle() : Battle
    {
        return $this->battle;
    }

    /**
     *
     * Dodanie rozgrywki
     *
     * @param Battle $battle
     */
    public function setBattle(Battle $battle)
    {
        $this->battle = $battle;
    }
}