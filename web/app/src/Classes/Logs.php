<?php

namespace Connect4\Classes;

use function GuzzleHttp\Psr7\str;

class Logs
{
    public $logs = array();

    const MOVE = 'Wrzucenie pastylki';
    const LOSETIME = 'Koniec czasu';
    const ADDPLAYER = 'Dodanie gracza';
    const STARTGAME = 'Tworzę grę';
    const MESSAGEVISIBLE = true;
    const MESSAGEUNVISIBLE = false;


    /**
     * Logs constructor.
     */
    public function __construct()
    {
        $this->logs = array(
            'visibility'    => array()
        );
    }

    /**
     *
     * Set logs action in battle
     *
     * @param $type
     * @param bool $visible
     * @param Player|null $player
     * @throws \Exception
     */
    public function add($type, bool $visible = true, Player $player = null) : void
    {
        $date = new \DateTime();

        $nickPlayer = null;
        if ($player) {
            $nickPlayer = $player->getNick();
        }

        $this->logs['visibility'][var_export($visible,true)][] = array(
            'type'  =>  $type,
            'player'  =>  $nickPlayer,
            'date'  =>  $date->setTimestamp(time()),
        );
    }

    /**
     *
     * Return logs from battle
     *
     * @return array
     */
    public function getLogs() : array
    {
        return $this->logs;
    }


}