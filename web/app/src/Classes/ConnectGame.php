<?php
namespace Connect4\Classes;
use Connect4\utils\Json;
use Noodlehaus\Config;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use Predis\Client;
use Monolog\Logger;


class ConnectGame implements MessageComponentInterface
{
    protected $battles;
    protected $players;
    protected $config;
    protected $redis;
    protected $logger;

    /**
     * ConnectGame constructor.
     * @param Config $config
     * @param Client $redis
     * @param Logger $logger
     * @throws \Exception
     */
    public function __construct(Config $config, Client $redis, Logger $logger)
    {
        $this->players = new \SplObjectStorage;
        $this->battles = new \SplObjectStorage;
        $this->config  = $config;
        $this->redis = $redis;
        $this->logger = $logger;
    }

    /**
     * @param ConnectionInterface $conn
     * @throws \Exception
     */
    public function onOpen(ConnectionInterface $conn)
    {
        $this->players->attach(
            $conn
        );
    }

    /**
     * @param ConnectionInterface $from
     * @param string $msg
     * @throws \Exception
     */
    public function onMessage(ConnectionInterface $from, $msg)
    {
        $message = Json::decode($msg);
        $this->logger->info('Wiadomosc przychodzaca: '.$msg, array(__METHOD__));
// todo - przeniesc logger do innych
        if($message->topic === $this->config->get('checker.topic')){

            $battleId = $message->payload->id;
            $this->logger->info('[connectGame] - sprawdzam battleId: '.$battleId);

            foreach ($this->battles as $battle){
                $this->logger->info('[connectGame] - porownuje battleId: '.$battleId .' z '. $battle->id);

                if($battle->id == $battleId && $battle->isMaxPlayerAchieved()){
                    $this->logger->info(
                        '[connectGame] - battleId: '. $battleId .' przeterminowany wywołuje metodę checkDeadline'
                    );

                    $battle->checkDeadline();
                }
            }
        } else if($message->topic === '/battles/join'){

            foreach ($this->players as $player){

                if($player === $from){
                    // TODO - SplObjectStorage on messanger FB
                    $playerId = $from->resourceId;
                    $newPlayer =  new Player($playerId, $from, $this->config, $this->logger);
//                    $this->players->attach(
//                        $from,
//                        $newPlayer
//                    );
                    $this->battles->attach(
                        $this->assignToBattle(
                            $newPlayer
                        )
                    );
                }
            }
        } else if($message->topic === $this->config->get('checker.topic')){

            $battleId = $message->payload->id;
            $this->logger->info('szukam battle: '.$battleId);

            foreach ($this->battles as $battle){
                $this->logger->info('battle do porownania: '. $battle->id);
                if($battle->id == $battleId){
                    $this->logger->info('battle znaleziony - wywołuje checkdeadline '. $battleId);
                    $battle->checkDeadline();
                }
                $this->logger->info('nie udalo sie znalezc battle '. $battleId);
            }
            $this->logger->info('koniec poszukiwań');
        } else {
            // TODO - przygotowane na przyszlosc do rozbudowy o kolejne tematy
            }
    }

    /**
     * @param ConnectionInterface $conn
     */
    public function onClose(ConnectionInterface $conn)
    {
        if ($this->players->offsetExists($conn)){

//            $player = $this->players->offsetGet($conn);

        }

        // The connection is closed, remove it, as we can no longer send it messages
        $this->players->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    /**
     * @param ConnectionInterface $conn
     * @param \Exception $e
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }

    /**
     *
     * Przypisanie zawodników do pola bitwy
     *
     * @param Player $player
     * @return Battle
     * @throws \Exception
     */
    private function assignToBattle(Player $player) : Battle
    {
        foreach ($this->battles as $battle){
            if (!$battle->isMaxPlayerAchieved()){

                $battle->addPlayer($player);

                return $battle;
            }
        }

        $battle = new Battle($player, new Board($this->config), $this->config, $this->redis, $this->logger);

        $this->redis->hset('battles:'.$battle->id, 'currentDeadline', null);
        /**
         * TODO
         * modyfikacja kodu gry w taki sposob aby stworzy warstwe abstrakcji na redisa
         * domyslnie przekazywany ma byc jakis obiekt db, ktory (co powinno wynikac z interface)
         * bedzie zawieral dwie metody set i get. Zachowanie seta ma byc bardzo identyczne do hset powyzej.
         */

        return $battle;
    }
}
