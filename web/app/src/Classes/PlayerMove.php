<?php

namespace Connect4\Classes;

class PlayerMove
{

    private $column;
    private $dateTime;
    private $player;
    private $row;

    /**
     * PlayerMove constructor.
     * @param Player $player
     * @param int $column
     * @throws \Exception
     */
    public function __construct(Player $player, int $column)
    {
        $this->setPlayer($player);
        $this->setColumn($column);
        $this->setDateTime();
    }

    /**
     * @return Player
     */
    public function getPlayer() : Player
    {
        return $this->player;
    }

    /**
     * @param Player $player
     */
    private function setPlayer(Player $player)
    {
        $this->player = $player;
    }

    /**
     * @return int
     */
    public function getColumn() : int
    {
        return $this->column;
    }

    /**
     * @param int $column
     */
    private function setColumn(int $column)
    {
        $this->column = $column;
    }

    /**
     * @return int
     */
    public function getRow() : int
    {
        return $this->row;
    }

    /**
     *
     * Metoda ustawiona jako publiczna, gdyz jest wykorzystywana w findRowKey w Border class
     *
     * @param int $row
     */
    public function setRow(int $row)
    {
        $this->row = $row;
    }

    /**
     * @return mixed
     */
    public function getDateTime() : \DateTime
    {
        return $this->dateTime;
    }

    /**
     * @throws \Exception
     */
    private function setDateTime()
    {
        $this->dateTime = new \DateTime();
    }
}