<?php

namespace Connect4\Classes;

use Connect4\exceptions\battle\BattleException;
use Connect4\exceptions\player\PlayerException;
use Connect4\utils\Json;
use Monolog\Logger;
use Noodlehaus\Config;
use Predis\Client;


class Battle
{
    public $board;
    public $id;
    public $players = array();

    private $config;
    private $colors = array();
    private $maxPlayers;
    private $timeToMove;
    private $currentDeadline;
    private $currentPlayer;
    private $awaitingPlayer;
    private $noActionCounterLimit;
    private $redis;
    private $logger;
    private $logs;

    /**
     * Battle constructor.
     * @param Player $player
     * @param Board $board
     * @param Config $config
     * @param Client $redis
     * @param Logger $logger
     * @throws \Exception
     */
    public function __construct(Player $player, Board $board, Config $config,  Client $redis, Logger $logger)
    {
        $this->config = $config;
        $this->id = rand(1,1000000);
        $this->board = $board;
        $this->redis = $redis;
        $this->logger = $logger;

        $this->initializeParams();
        $this->initializeColors();
        $this->initializeLogs();
        $this->addPlayer($player);
    }

    /**
     *
     */
    private function initializeParams()
    {
        $this->maxPlayers = $this->config->get('battle.maxPlayers');
        $this->timeToMove = $this->config->get('battle.timeToMove');
        $this->noActionCounterLimit = $this->config->get('battle.noActionCounterLimit');
    }

    /**
     *
     * Add initialize battle to logs
     *
     * @throws \Exception
     */
    private function initializeLogs() : void
    {
        $this->setLogs(new Logs());
        $this->getLogs()->add(Logs::STARTGAME, Logs::MESSAGEVISIBLE);
    }

    /**
     * @param $log
     */
    public function setLogs( Logs $log)
    {
        $this->logs = $log;
    }

    /**
     * @return mixed
     */
    public function getLogs() : Logs
    {
        return $this->logs;
    }

    /**
     * Dodanie zawodnika wraz z przydzieleniem mu koloru
     *
     * @param Player $player
     * @throws \Exception
     */
    public function addPlayer(Player $player)
    {
        if(!$this->isMaxPlayerAchieved()){
            $color = $this->getColor();

            $player->setBattle($this);
            $newPlayer = new \stdClass();
            $newPlayer->raw_object = $player;
            $newPlayer->color = $color;
            $newPlayer->no_action_counter = 0;
            $newPlayer->nick = $player->getNick();

            $this->getLogs()->add(Logs::ADDPLAYER, Logs::MESSAGEVISIBLE, $player);

            if($this->isPlayerUnique($player))
            {
                $this->players[] = $newPlayer;
                $this->getPlayers();
                $player->send(
                    Json::encode(
                        array(
                            'topic' => 'init',
                            'payload'   => array(
                                'color' =>  $color,
                                'matrix' => $this->board->getFormattedMatrix(),
                                'logs'   => $this->getLogs(),
                                'nick'  =>  $player->getNick(),
                                'battleId'  => $this->getBattleId(),
                                'players'   => $this->getPlayers(),
                            )
                        )
                    )
                );
                $this->beginBattle();
            } else {
                throw new PlayerException('Player is not unique');
            }
        } else {
            throw new PlayerException('Cannot add player');
        }
    }

    /**
     *
     * Sprawdza czy zostala osiagnieta maksymalna ilosc graczy
     *
     * @return bool
     */
    public function isMaxPlayerAchieved() : bool
    {
        return \count($this->players) === $this->getMaxPlayers();
    }

    /**
     *
     * Pobranie aktualnego gracza
     *
     * @return null
     */
    public function getCurrentPlayer()
    {
        return $this->currentPlayer;
    }

    /**
     *
     * Pobranie czasu do wykonania ruchu
     *
     * @return float|int
     */
    public function getTimeToMove()
    {
        return $this->timeToMove;
    }

    public function getBattleId()
    {
        return $this->id;
    }

    /**
     *
     * Pobranie czas
     *
     * @return null
     */
    public function getCurrentDeadline()
    {
        return $this->currentDeadline;
    }

    /**
     *
     * Pobiera ilość pustych ruchów
     *
     * @return int
     */
    public function getNoActionCounterLimit() : int
    {
        return $this->noActionCounterLimit;
    }

    /**
     *
     * Pobieranie maksymalnej ilości graczy
     *
     * @return int
     */
    public function getMaxPlayers() : int
    {
        return $this->maxPlayers;
    }

    /**
     *
     * Sprawdzenie czy nie został przekroczony deadline
     * Jak tak to przełącza gracza
     *
     * @throws \Exception
     */
    public function checkDeadline()
    {
        $date = new \DateTime();

        $currentDeadLineTs = $this->getCurrentDeadline()->getTimestamp();
        $this->logger->info('Battle: '. $this->getBattleId(). ' deadline: '.$currentDeadLineTs);
        if($date->getTimestamp() > $currentDeadLineTs){
            $player = $this->getCurrentPlayer();
            $player->no_action_counter++;
            $this->checkPlayerCounter();
            $this->setPlayer();
            $this->logger->info('Koniec czasu na ruch');
        }
    }

    /**
     * Pobranie kolorów dla zawodników
     *
     * @return mixed
     */
    private function getColor()
    {
        return array_shift($this->colors);
    }

    /**
     * Generowanie zestawu kolorów
     */
    private function initializeColors()
    {
       $this->colors = range(1, $this->getMaxPlayers());
    }

    /**
     *
     * Rozpoczęcie gry
     *
     * @throws \Exception
     */
    private function beginBattle()
    {
        if($this->isMaxPlayerAchieved()){
            $this->setPlayer();
        }
    }

    /**
     *
     * Ustawienie gracza

     *
     * @throws \Exception
     */
    private function setPlayer()
    {
        $this->setCurrentDeadline();
        $this->setCurrentAndAwaitingPlayer();
        $this->broadcastBattleData();
    }

    /**
     *
     * Wysłanie informacji o ruchach w grze do wszystkich graczy
     *
     */
    private function broadcastBattleData()
    {
        foreach ($this->players as $player){
            $player->raw_object->send(
                Json::encode(
                    array(
                        'topic' => 'currentBattleData',
                        'payload' =>  array(
                            'deadline'  =>  $this->getCurrentDeadline()->getTimestamp(),
                            'player'    =>  $this->getCurrentPlayer(),
                            'matrix'    =>  $this->board->getFormattedMatrix(),
                            'logs'      =>  $this->getLogs(),
                            'players'   =>  $this->getPlayers(),
                        )
                    )
                )
            );
        }
    }

    /**
     *
     * Ustawienie czasu deadline
     *
     * @throws \Exception
     */
    private function setCurrentDeadline()
    {
        $date = new \DateTime();
        $date->add(new \DateInterval('PT'.$this->getTimeToMove().'S'));

        $this->redis->hset('battles:'.$this->id, 'currentDeadline', $date->getTimestamp());

        $this->currentDeadline = $date;
    }

    /**
     *
     * Usawienie aktualnego gracza
     *
     */
    private function setCurrentAndAwaitingPlayer()
    {
        if ($this->getCurrentPlayer()=== null){
            try{
                $currentPlayerArrayOffset = random_int(0,$this->getMaxPlayers()-1);

                $this->currentPlayer = $this->getPlayer($currentPlayerArrayOffset);

                $nextPlayerArrayOffset = $currentPlayerArrayOffset + 1;
                if ($nextPlayerArrayOffset >= $this->getMaxPlayers()){
                    $this->awaitingPlayer = $this->getPlayer(0);
                } else {
                    $this->awaitingPlayer = $this->getPlayer($nextPlayerArrayOffset);
                }
            } catch (\Exception $e){
                $this->currentPlayer = $this->getPlayer(0);
                $this->awaitingPlayer = $this->getPlayer(1);
            }
        } else {
            $currentPlayerArrayOffset = array_search(
                $this->currentPlayer,
                $this->players,
                true
            );
            $nextPlayerArrayOffset = $currentPlayerArrayOffset + 1;
            if ($nextPlayerArrayOffset >= $this->getMaxPlayers()){
                $this->currentPlayer = $this->getPlayer(0);
                $this->awaitingPlayer = $this->getPlayer(1);
            } else {
                $this->currentPlayer = $this->getPlayer($nextPlayerArrayOffset);
                $this->awaitingPlayer = $this->getPlayer($currentPlayerArrayOffset);
            }
        }
    }

    /**
     *
     * Sprawdzenie czy gracz jest unikalny
     *
     * @param Player $player
     * @return bool
     *
     */
    private function isPlayerUnique(Player $player) : bool
    {
        foreach ($this->players as $addedPlayer){
            if($addedPlayer->raw_object === $player){
                return false;
            }
        }
        return true;
    }

    /**
     *
     * Sprawdzamy czy maksymalna ilosc pustych ruchow nie zostala przekroczona.
     * Jak tak to wysylana jest do graczy informacja kto przegral
     *
     * @throws \Exception
     */
    private function checkPlayerCounter()
    {
        $currentPlayer = $this->getCurrentPlayer();
        if($currentPlayer->no_action_counter === $this->getNoActionCounterLimit()){
            foreach ($this->players as $player){
                $player->raw_object->send(
                    Json::encode(
                        array(
                            'topic' => 'looser',
                            'payload' =>     array(
                                'message' =>  'Przegrał player - ' .
                                    $currentPlayer->raw_object->getNick(),
                            )
                        )
                    )
                );
            }
            $this->redis->DEL('battles:'.$this->id);
            throw new BattleException('Gra zakończona');
        }
    }

    /**
     *
     * Pobranie listy graczy
     *
     * @param bool $allData
     * @return array
     */
    public function getPlayers($allData = false)
    {
        $playersList = array();

        foreach ($this->players as $player){
            if($allData){
                $playersList[] = $player->raw_object;
            } else {
                $playersList[] = $player->raw_object->getNick();
            }
        }
        return $playersList;
    }

    /**
     *
     * Pobranie player na podstawie przesłanego indeksu, lub jesli index == null pobierany jest currentPlayer
     *
     * @param $offset
     * @return mixed|null
     */
    public function getPlayer($offset = null)
    {
        if(!is_null($offset)){
            return $this->players[$offset];
        }

        return $this->getCurrentPlayer();
    }
}