<?php

namespace Connect4\tests;

use Connect4\Classes\Player;
use PHPUnit\Framework\TestCase;
use Connect4\exceptions\NotImplementedException;
use Ratchet\ConnectionInterface;
use Noodlehaus\Config;
use Noodlehaus\Parser\Yaml;
use Monolog\Logger;


class PlayerTest extends TestCase
{
    protected $config;

    private $connection;
    private $logger;


    /**
     *
     * Tworzenie ustawień testów
     *
     */
    public function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        parent::setUp();
        $this->connection = $this
            ->getMockBuilder(ConnectionInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->config = new Config(__DIR__ . '/../config/test.yml', new Yaml);

        $this->logger = $this
            ->getMockBuilder(Logger::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * test metody generateNick
     */
    public function test_generateNick()
    {
        $player = new Player(1,$this->connection, $this->config, $this->logger);

        $this->assertEquals(
            $player->getDefaultNickLength(),
            \strlen($player->getNick())
        );
    }

    /**
     * test metody getId
     */
    public function test_getId()
    {
        $this->expectException(\TypeError::class);

        new Player($this->connection, $this->connection, $this->config, $this->logger);
        $player = new Player(1, $this->connection, $this->config, $this->logger);
        $this->assertEquals(1, $player->getId());
    }

    /**
     *
     * test metody send
     *
     * @throws \Exception
     */
    public function test_send()
    {
        $conn = $this->getMockBuilder(ConnectionInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $conn->method('send')->willReturn(true);
        $player = new Player((int) 3, $conn, $this->config, $this->logger);
        $this->assertTrue( $player->send($this->connection));

        $conn = $this->getMockBuilder(ConnectionInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $conn->method('send')->willReturn(false);
        $player = new Player((int) 3, $conn, $this->config, $this->logger);
        $this->assertFalse( $player->send($this->connection));

        try {
            $player = new Player((int) 3, new \stdClass(), $this->config, $this->logger);
            $player->send($this->connection);
        } catch (NotImplementedException $e) {
            $answer = $e->getMessage();
        }
        $this->assertEquals
        (
            $answer,
            'Brak implementacji dla klasy: stdClass'
        );

        try {
            new Player((int) 3, '', $this->config, $this->logger);
        } catch (\Exception $e) {
            $answer = $e->getMessage();
        }
        $this->assertEquals
        (
            $answer,
            'Przesłana wartość nie jest obiektem'
        );
    }
}
