<?php

namespace Connect4\tests;
use Connect4\Classes\Battle;
use Connect4\Classes\Board;
use Connect4\exceptions\battle\BattleException;
use Connect4\Classes\Player;
use PHPUnit\Framework\TestCase;
use Predis\Client;
use Ratchet\ConnectionInterface;
use Noodlehaus\Config;
use Noodlehaus\Parser\Yaml;
use Monolog\Logger;



class BattleTest extends TestCase
{
    protected $player;
    protected $board;
    protected $config;
    protected $redis;
    protected $logger;


    /**
     * Setup Battletest
     */
    protected function setUp()
    {

        $this->redis =  new Client([
            'scheme' => 'tcp',
            'host'   => 'redis',
            'port'   => 6379,
        ]);
        $this->logger = $this
            ->getMockBuilder(Logger::class)
            ->disableOriginalConstructor()
            ->getMock();

        $conn = $this->getMockBuilder(ConnectionInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $conn->method('send')->willReturn(true);

        $this->config = new Config(__DIR__ . '/../config/test.yml', new Yaml);
        $this->player1 = new Player((int) 1, $conn, $this->config, $this->logger);
        $this->player2 = new Player((int) 2, $conn, $this->config, $this->logger);
        $this->player3 = new Player((int) 3, $conn, $this->config, $this->logger);
        $this->board =
            $this->
            getMockBuilder(Board::class)->
            disableOriginalConstructor()->
            getMock()
        ;
    }

    /**
     *
     * test klasy addPlayer
     *
     * @throws \Exception
     */
    public function test_addPlayer()
    {

        $battle = new Battle($this->player1, $this->board, $this->config, $this->redis, $this->logger);

        $this->assertNotEquals(null, $this->player1->getBattle());
//        $this->assertEquals(1, $battle->players[0]->color);
//            try{
//                $battle->addPlayer($this->player1);
//            }catch (PlayerException $e){
//                $this->assertEquals
//                (
//                    'Player is not unique',
//                    $e->getMessage()
//                );
//            }
//        try{
//            $battle->addPlayer($this->player2);
//            $battle->addPlayer($this->player3);
//        }catch (PlayerException $e){
//            $this->assertEquals('Cannot add player', $e->getMessage());
//        }
    }

    /**
     *
     * test klasy isMaxPlayerArchieved
     *
     * @throws \Exception
     */
    public function test_isMaxPlayerAchieved()
    {
        $battle = new Battle($this->player1, $this->board, $this->config, $this->redis, $this->logger);
        $this->assertFalse($battle->isMaxPlayerAchieved());
        $battle->addPlayer($this->player2);
        $this->assertTrue($battle->isMaxPlayerAchieved());
    }

    /**
     *
     * Pobranie czasu current deadline
     *
     * @throws \Exception
     */
    public function test_getCurrentDeadline()
    {

        $battle = new Battle($this->player1, $this->board, $this->config, $this->redis, $this->logger);
        $this->assertEquals(null, $battle->getCurrentDeadline());
        $battle->addPlayer($this->player2);
        $this->assertEquals(
            \DateTime::class,
            get_class($battle->getCurrentDeadline())
        );
    }

    /**
     *
     * Pobranie aktualnego gracza
     *
     * @throws \Exception
     */
    public function test_getCurrentPlayer()
    {
        $battle = new Battle($this->player1, $this->board, $this->config, $this->redis, $this->logger);
        $this->assertEquals(null, $battle->getCurrentPlayer());
        $battle->addPlayer($this->player2);
        $asdcasc = $battle->getCurrentPlayer();
        $this->assertEquals(
            Player::class,
            get_class($battle->getCurrentPlayer()->raw_object)
        );
    }

    /**
     *
     * Sprawdzenie działania przełączenia na nastepnego gracza
     * po upływie Deadline
     *
     * @throws \Exception
     */
    public function test_checkDeadline()
    {
        $battle = new Battle($this->player1, $this->board, $this->config, $this->redis, $this->logger);
        $battle->addPlayer($this->player2);
        $currentPlayer = $battle->getCurrentPlayer();
        sleep($battle->getTimeToMove() + 1);
        $battle->checkDeadline();
        $this->assertNotEquals($currentPlayer, $battle->getCurrentPlayer());

        $noActionCounterLimit = $battle->getNoActionCounterLimit();

        for($i=0; $i < $noActionCounterLimit * $battle->getMaxPlayers(); $i++)
        {
            try{
                sleep($battle->getTimeToMove() + 1);
                $battle->checkDeadline();
            }catch (BattleException $e){
                $this->assertEquals('Gra zakończona', $e->getMessage());
            }
        }
    }

    protected function tearDown()
    {
        $conn = null;
    }
}