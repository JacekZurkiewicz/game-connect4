<?php
/**
 * Created by PhpStorm.
 * User: jacek
 * Date: 08.11.18
 * Time: 00:33
 */

namespace Connect4\tests;

use Connect4\utils\BattleChecker;
use PHPUnit\Framework\TestCase;
use Predis\Client;

class BattleCheckerTest extends TestCase
{

    private $redis;

    protected function setUp()
    {
        $this->redis =  new Client([
            'scheme' => 'tcp',
            'host'   => 'redis',
            'port'   => 6379,
        ]);
    }


    /**
     *
     * Sprawdzanie poprawności pobierania numerów rozgrywek
     *
     */
    public function testGetDeadlinedBattlesId()
    {
        $battleChecker = new BattleChecker($this->redis);

        $this->redis->hset('battles:101', 'currentDeadline', time() - 500);
        $this->redis->hset('battles:102', 'currentDeadline', time() + 500);
        $this->redis->hset('battles:103', 'currentDeadline', time() - 100);
        $this->redis->hset('battles:104', 'currentDeadline', time() - 100);
        $this->redis->hset('battles:105', 'currentDeadline', null);

        $this->assertContains('101', $battleChecker->getDeadlinedBattlesId());
        $this->assertNotContains('105', $battleChecker->getDeadlinedBattlesId());
    }
}
