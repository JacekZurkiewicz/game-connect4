<?php

namespace Connect4\tests;

use Connect4\Classes\Player;
use Connect4\Classes\Board;
use Connect4\Classes\PlayerMove;
use Connect4\exceptions\board\BoardException;
use PHPUnit\Framework\TestCase;
use Ratchet\ConnectionInterface;
use Noodlehaus\Config;
use Noodlehaus\Parser\Yaml;
use Monolog\Logger;


class BoardTest extends TestCase
{

    protected $config;

    private $connection;
    private $logger;

    protected function setUp()
    {
        parent::setUp();
        $this->connection = $this
            ->getMockBuilder(ConnectionInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->config = new Config(__DIR__ . '/../config/test.yml', new Yaml);

        $this->logger = $this
            ->getMockBuilder(Logger::class)
            ->disableOriginalConstructor()
            ->getMock();
    }
    /**
     *
     * test konstruktora
     *
     */
    public function test__construct()
    {
        $board = new Board($this->config);
        $this->assertInternalType('array', $board->matrix);
        $this->assertCount($board->getSize(), $board->matrix);

        foreach ($board->matrix as $row){

            $this->assertCount($board->getSize(), $row);

        }
    }

    /**
     * test funkcji getFormattedMatrix
     */
    public function test_getFormattedMatrix()
    {
        $board = new Board($this->config);
        $this->assertEquals($board->matrix, $board->getFormattedMatrix());
    }

    /**
     *
     * test funkcji makeMove
     *
     * @throws \Connect4\exceptions\NotImplementedException
     * @throws \Connect4\exceptions\NotObject
     * @throws \ReflectionException
     */
    public function test_makeMove()
    {
        $board = new Board($this->config);
        $playerMove =  new PlayerMove(
            new Player(1,$this->connection, $this->config, $this->logger),
            6
        );

        $this->assertEquals(
            true,
            $board->makeMove(
                $playerMove
            )
        );

        $this->assertEquals(
            $board->getSize(),
            $playerMove->getRow()
        );

        $playerMove =  new PlayerMove(
            new Player(1,$this->connection, $this->config, $this->logger),
            6
        );

        $this->assertEquals(
            true,
            $board->makeMove(
                $playerMove
            )
        );

        $this->assertEquals(
            $board->getSize()-1,
            $playerMove->getRow()
        );

        try{
            $board->makeMove(
                new PlayerMove(
                    new Player(1,$this->connection, $this->config, $this->logger),
                    11
                )
            );
        }catch (BoardException $e){
            $this->assertEquals('Błędny numer kolumny', $e->getMessage());
        }

        try{
            for ($x=0;$x<=$board->getSize();$x++){
                $playerMove =  new PlayerMove(
                    new Player(1,$this->connection, $this->config, $this->logger),
                    2
                );
                $board->makeMove(
                    $playerMove
                );
            }
        }catch (BoardException $e){
            $this->assertEquals('Zapełniona kolumna', $e->getMessage());
        }
    }
}
