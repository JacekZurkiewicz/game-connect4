<?php
/**
 * Created by PhpStorm.
 * User: jacek
 * Date: 04.10.18
 * Time: 14:37
 */

namespace Connect4\tests;

use Connect4\utils\Json;
use PHPUnit\Framework\TestCase;

class JsonTest extends TestCase
{

    /**
     *
     * Test kodowania json
     *
     */
    public function testEncode()
    {

        $data = array(
            'topic' => 'test',
            'payload' => array ('a'=>1,'b'=>2,'c'=>3)
        );

        $this->assertEquals(
            "{\"topic\":\"test\",\"payload\":{\"a\":1,\"b\":2,\"c\":3}}",
            Json::encode($data)
        );

    }

    /**
     *
     * Test rozpakowanie json
     *
     */
    public function testDecode()
    {
        $this->assertEquals(
            array ('a'=>1,'b'=>2,'c'=>3),
            Json::decode('{"a":1,"b":2,"c":3}', true)
        );

        $expectedObject = new \stdClass();
        $expectedObject->a = 1;
        $expectedObject->b = 2;
        $expectedObject->c = 3;

        $this->assertEquals(
            $expectedObject,
            Json::decode('{"a":1,"b":2,"c":3}', false)
        );
    }
}
