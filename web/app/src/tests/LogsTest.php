<?php

namespace Connect4\tests;

use Connect4\Classes\Logs;
use PHPUnit\Framework\TestCase;


class LogsTest extends TestCase
{
    private $date;

    protected function setUp()
    {
        $this->date = new \DateTime();
        $this->date->setTimestamp(time());
    }

    /**
     *
     * Test dodawania logów do historii
     *
     * @throws \Exception
     */
    public function test_add()
    {
        $logs = new Logs();
        $logs->add(Logs::STARTGAME, Logs::MESSAGEVISIBLE);

        $expected['visibility'][var_export(Logs::MESSAGEVISIBLE,true)][] = [
            'type'  =>  Logs::STARTGAME,
            'player'  =>  null,
        ];

        $actual = $logs->getLogs();
        unset($actual['date']);

        $this->assertArraySubset($expected, $actual);
    }
}