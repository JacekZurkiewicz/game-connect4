<?php

namespace Connect4;

use Noodlehaus\Config;
use Noodlehaus\Parser\Yaml;

$settingsYaml = <<<FOOBAR
board:
    size: 10
battle:
    maxPlayers: 3
    timeToMove: 5*60;
    noActionCounterLimit = 3;

FOOBAR;

$conf = new Config($settingsYaml, new Yaml, true);