<?php

namespace Connect4\db\interfaces;


interface db
{
    /**
     * @return mixed
     */
    public function get();

    /**
     * @param $action
     * @param $params
     * @return mixed
     */
    public function set($action, $params);

    /**
     * @param $data
     * @return mixed
     */
    public function delete($data);
}