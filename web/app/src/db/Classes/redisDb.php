<?php


namespace Connect4\db\Classes;


use Connect4\db\interfaces\db;
use Predis\Client;

class redisDb implements db
{
    const ADDPLAYER;

    public $redis;

    public function __construct(Client $redis)
    {
        $this->redis = $redis;
    }

    public function get()
    {
        // TODO: Implement get() method.
    }

    public function set($action, $params)
    {
        if($action === self::ADDPLAYER){
            $this->addPlayer($params);
        }
    }

    public function delete($data)
    {
        // TODO: Implement delete() method.
    }

    public function addPlayer($params)
    {
        $this->redis->hset('battles:'.$params->id, 'currentDeadline', $params->getTimestamp());
    }
}