#!/usr/bin/env sh

set -eu

apk update
apk upgrade
apk add python python-dev py-pip build-base libffi-dev openssl-dev libgcc

  # Add python pip and bash
apk add --no-cache py-pip bash

  # Install docker-compose via pip
pip install --no-cache-dir docker-compose
docker-compose